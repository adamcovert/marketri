$(document).ready(function(){

  svg4everybody();

  function $$(selector, context) {
    context = context || document;
    var elements = context.querySelectorAll(selector);
    return Array.prototype.slice.call(elements);
  };

  var burgers = $$('.hamburger');

  for (var i = 0; i < burgers.length; i++) {
    var burger = burgers[i];
    burger.addEventListener('click', showBurgerTarget);

    function showBurgerTarget() {
      var targetId = this.getAttribute('data-target-id');
      var targetClassToggle = this.getAttribute('data-target-class-toggle');

      if (targetId && targetClassToggle) {
        this.classList.toggle('hamburger--is-active');
        document.getElementById(targetId).classList.toggle(targetClassToggle);

        if (document.getElementById(targetId).classList.contains('mobile-menu--is-shown')) {
          document.querySelector('body').classList.add('prevent-scroll');
        } else {
          document.querySelector('body').classList.remove('prevent-scroll');
        }
      }
    }
  };
});