$(document).ready(function() {

  $('#promo-slider').slick({
    prevArrow: '<button type="button" class="slick-prev"><svg width="20px" height="36px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#000" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg></button>',
    nextArrow: '<button type="button" class="slick-next"><svg width="20px" height="36px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#000" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg></button>'
  });

  $('.s-main-slider').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<button type="button" class="slick-prev"><svg width="20px" height="36px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#000" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg></button>',
    nextArrow: '<button type="button" class="slick-next"><svg width="20px" height="36px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#000" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg></button>',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true
        }
      }
    ]
  });


  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
  });

  $('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: true,
    focusOnSelect: true,
    centerPadding: '50px',
    arrows: false
  });

});