$(document).ready(function() {

  $('.s-mobile-menu__nav-item--has-child').each(function () {
    $(this).find('.s-mobile-menu__nav-link').on('click', function (e) {
      e.preventDefault();
      $(this).parent().find('.s-mobile-menu__sublist-wrapper').toggleClass('is-open');
      $(this).parent().toggleClass('s-mobile-menu__nav-item--sublist-is-open');
    })
  });
});